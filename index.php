<?php

/**
 * Archivo index. Es el primer archivo que devuelve el servidor. En funcion de la cookie de sesion, renderiza una pagina
 * u otra
 * @author Jose Francisco Artigas Alc�zar
 * @version 1.0
 */
ini_set ( "error_reporting", "true" );
error_reporting ( E_ALL );

require_once 'lib/config.php';
require_once 'lib/control.php';

session_start ();

if (! isset ( $_SESSION ['user'] )) {
	header ( "Location: login.php" );
}

$twig = config_twig ();
$twig->addGlobal ( "session", $_SESSION );
$template = $twig->loadTemplate ( "home.html" );
$recetas = listatodas ();
$datos = array (
		'datos' => $recetas,
		'nombre' => $_COOKIE ['nickUser'],
		'header' => 'Ultimas recetas publicadas' 
);
echo $template->render ( $datos );
?>