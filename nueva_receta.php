<?php

/**
 * Archivo que recibe los datos del formulario de nueva receta y lo reenvia al archivo de control para procesarlos
 * e insertarlos en la base de datos. Permite guardar imagenes
 * @author Jose Francisco Artigas Alc�zar
 * @version 1.0
 */
include 'lib/control.php';
require_once 'lib/config.php';

if (isset ( $_POST ['iNombreReceta'] ) && isset ( $_POST ['sTipoReceta'] ) && isset ( $_POST ['taElaboracionReceta'] ) && isset ( $_POST ['iTiempoElaboracion'] ) && isset ( $_POST ['taIngredientes'] ) && isset ( $_FILES ['fImagen'] )) {
	
	$rutaFoto = "http://localhost/RecetarioPHP/imagenes/";
	$temp = $_FILES ['fImagen'] ['tmp_name'];
	$name = $_FILES ['fImagen'] ['name'];
	$tama�oBytes = $_FILES ['fImagen'] ['size'];
	$typeFile = $_FILES ['fImagen'] ['type'];
	$kiloBytes = $tama�oBytes / 1024; // esto nos da la cantidad de kb
	if ($kiloBytes > 300) {
		echo "El archivo supera los 300 KB &lt;br/&gt;";
		sleep ( 2 );
		header ( "Location: nueva_receta.php" );
	}
	
	if ($typeFile == "image/jpeg" || $tipoFile == "image/gif" || $tipoFile == "image/png") {
		echo "Es el tipo esperado &lt;br/&gt;";
	} else {
		echo "Archivo no v�lido";
		header ( "Location: nueva_receta.php" );
	}
	
	switch ($typeFile) {
		case 'image/jpeg' :
			$ext = ".jpg";
			break;
		case 'image/gif' :
			$ext = ".gif";
			break;
		case 'image/png' :
			$ext = ".png";
			break;
	}
	
	// VALOR ALEATORIO CON EL QUE SE ALMACENAR� EL ARCHIVO
	$str = $_COOKIE ['nickUser'];
	$cad = "";
	// Se establece una longitu de 18
	for($i = 0; $i < 18; $i ++) {
		$cad .= substr ( $str, rand ( 0, 62 ), 1 );
	}
	
	$alea1 = $cad . $ext;
	$alea1 = str_replace ( " ", "_", $alea1 );
	
	$nuevaReceta = nuevaReceta ( $_POST ['iNombreReceta'], $_POST ['sTipoReceta'], $_POST ['taElaboracionReceta'], $_POST ['iTiempoElaboracion'], $_POST ['taIngredientes'], $rutaFoto . $alea1 );
	// Ruta al directorio que guardar� las im�genes
	$dir = "imagenes/";
	
	// Una vez guardada la imagen, mueve la foto al directorio indicado
	move_uploaded_file ( $temp, "$dir/$alea1" );
	header ( "Location: privado.php" );
} elseif (! isset ( $_COOKIE ['nickUser'] )) {
	header ( "Location: index.php" );
} else {
	$twig = config_twig ();
	$template = $twig->loadTemplate ( "nueva_receta.html" );
	$datos = array ();
	echo $template->render ( $datos );
}

?>
