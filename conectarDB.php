<?php
/**
 * Archivo de conexion a la base de datos
 * @author Jose Francisco Artigas Alc�zar
 * @version 1.0
 */
function conectar() {
	try {
		$conexion = new PDO ( "sqlite:db.sqlite3" );
		$conexion->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		return $conexion;
	} catch ( PDOException $e ) {
		echo 'ERROR: ' . $e->getMessage ();
		return false;
	}
}
?>