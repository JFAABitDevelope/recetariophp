<?php
/**
 * Archivo de lugar privado. Cada usuario solo tiene acceso a su lugar privado y no al lugar privado de otros
 * usuarios.
 * @author Jose Francisco Artigas Alc�zar
 * @version 1.0
 */
include 'lib/config.php';
include 'lib/control.php';

if (isset ( $_POST ['id_receta'] ) && isset ( $_POST ['id_usuario'] ) && isset ( $_POST ['nombre_receta'] ) && isset ( $_POST ['tipo_receta'] ) && isset ( $_POST ['elaboracion'] ) && isset ( $_POST ['tiempo_elaboracion'] ) && isset ( $_POST ['fecha_creacion'] ) && isset ( $_POST ['fecha_ult_mod'] ) && isset ( $_POST ['ingredientes'] ) && isset ( $_POST ['foto'] )) {
} elseif (! isset ( $_COOKIE ['nickUser'] )) {
	header ( "Location: index.php" );
} elseif (isset ( $_GET ['id'] )) {
	borrarSeleccionado ( $_GET ['id'] );
	header ( "Location: privado.php" );
} else {
	$misrecetas = getMisRecetas ( $_COOKIE ['nickUser'] );
	if ($misrecetas == false) {
		$no_recetas = 'Aun no tienes recetas';
	} else {
		$no_recetas = "Estas son tus recetas";
	}
	
	$twig = config_twig ();
	$template = $twig->loadTemplate ( "mis_recetas.html" );
	$datos = array (
			'nombre' => $_COOKIE ['nickUser'],
			'recetas' => $misrecetas,
			'no_recetas' => $no_recetas 
	);
	echo $template->render ( $datos );
}
?>