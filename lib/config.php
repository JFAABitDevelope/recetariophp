<?php
$root = "root";
$passwd_admin = "root";

/**
 * Archivo de configuracion
 * 
 * @author Jose Francisco Artigas Alc�zar
 * @version 1.0
 */

// Funcion que devuelve un objeto Twig. Se usar� para renderizar plantillas
function config_twig() {
	require_once "/vendor/autoload.php"; // Carga autom�ticamente
	//require_once "/vendor/twig/twig/lib/Twig/Autoloader.php";
	Twig_Autoloader::register ();
	$loader = new Twig_Loader_Filesystem ( realpath ( dirname ( __FILE__ ) . "../../vistas/" ) );
	$twig = new Twig_Environment ( $loader );
	$escaper = new Twig_Extension_Escaper ( true );
	$twig->addExtension ( $escaper );
	$twig->getExtension ( 'core' )->setTimezone ( 'Europe/Madrid' );
	return $twig;
}

// Funcion para generar un hash para el administrador
function generateHash($passwd_admin) {
	if (defined ( "CRYPT_BLOWFISH" ) && CRYPT_BLOWFISH) {
		$salt = '$2y$11$' . substr ( md5 ( uniqid ( rand (), true ) ), 0, 22 );
		return crypt ( $passwd_admin, $salt );
	}
}
$admin_pass = generateHash ( $passwd_admin );
?>