<?php

/**
 * Archivo de control.
 * Se realizan todas las operaciones con la base de datos SQLite
 * @author Jose Francisco Artigas Alc�zar
 * @version 1.0
 */
include 'conectarDB.php';
global $conexion;
function borrarSeleccionado($id) {
	$conexion = conectar ();
	$conexion->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	$sql = "DELETE FROM appRecetario_receta WHERE id=" . $id;
	$result = $conexion->prepare ( $sql );
	$result->execute ();
	$conexion = null;
}
function modificarReceta($id, $nombre, $tipo, $elaboracion, $tiempoAprox, $ingredientes) {
	$conexion = conectar ();
	$sql = "update appRecetario_receta set nombre=?, tipo=?, elaboracion=?, tiempo_aprox_elab=?, ingredientes=?
				 WHERE id=?;";
	$result = $conexion->prepare ( $sql );
	$result->execute ( array (
			$nombre,
			$tipo,
			$elaboracion,
			$tiempoAprox,
			$ingredientes,
			$id 
	) );
	$conexion = null;
	
	header ( "Location: privado.php" );
}
function listatodas() {
	$conexion = conectar ();
	$conexion->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	$sql = "SELECT * FROM appRecetario_receta order by fecha_creacion desc";
	$result = $conexion->prepare ( $sql );
	$result->execute ();
	$conexion = null;
	return $result;
}

/**
 * Utiliza query($sql) para devolver un vector con los resultados obtenidos
 * Devuelve false en caso de no obtener resultados
 * 
 * @return array
 */
function getNumRecetas() {
	$conexion = conectar ();
	$conexion->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	$sql = "SELECT COUNT(*) FROM appRecetario_receta";
	$result = $conexion->query ( $sql );
	while ( $resultado = $result->fetch () ) {
		$num_recetas = $resultado [0];
		$conexion = null;
		return $num_recetas;
	}
}

/**
 * No se crean usuarios, pero se almacenan recetas cuyo campo usuario_id guarda el valor de
 * la cookie que guarda el id del usuario
 * 
 * @param nickUser $username        	
 * @return PDOStatement
 */
function getMisRecetas($username) {
	$conexion = conectar ();
	$conexion->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	$id = $_COOKIE ['idUser'];
	$sql = "SELECT * FROM appRecetario_receta
					WHERE appRecetario_receta.usuario_id = '" . $id . "'";
	$result = $conexion->query ( $sql );
	$conexion = null;
	return $result;
}

/**
 * Funcion que guarda una receta.
 * Se utiliza un formato de fecha sem�ntico para f�cil lectura
 * 
 * @param String $nombre        	
 * @param String $tipo        	
 * @param String $elaboracion        	
 * @param String $tiempoElab        	
 * @param String $ingredientes        	
 * @param file $foto        	
 */
function nuevaReceta($nombre, $tipo, $elaboracion, $tiempoElab, $ingredientes, $foto) {
	$formatoF = "d/M/Y";
	$fechaHoy = date ( $formatoF );
	
	$conexion = conectar ();
	$conexion->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	
	$id = $_COOKIE ['idUser'];
	$sql = "INSERT INTO appRecetario_receta (id, usuario_id, nombre, tipo, elaboracion, tiempo_aprox_elab,
			fecha_creacion, fecha_ult_mod, ingredientes, foto) VALUES (:id, :usuario_id, :nombre, :tipo,
			 :elaboracion, :tiempo_aprox_elab, :fecha_creacion, :fecha_ult_mod, :ingredientes, :foto);";
	
	$result = $conexion->prepare ( $sql );
	$result->bindParam ( ':usuario_id', $id, PDO::PARAM_INT );
	$result->bindParam ( ':nombre', ucwords ( $nombre ), PDO::PARAM_STR );
	$result->bindParam ( ':tipo', $tipo, PDO::PARAM_STR );
	$result->bindParam ( ':elaboracion', $elaboracion, PDO::PARAM_STR );
	$result->bindParam ( ':tiempo_aprox_elab', $tiempoElab, PDO::PARAM_STR );
	$result->bindParam ( ':fecha_creacion', $fechaHoy, PDO::PARAM_STR );
	$result->bindParam ( ':fecha_ult_mod', $fechaHoy, PDO::PARAM_STR );
	$result->bindParam ( ':ingredientes', $ingredientes, PDO::PARAM_STR );
	$result->bindParam ( ':foto', $foto, PDO::PARAM_STR );
	
	$result->execute ();
	$conexion = null;
	
	header ( "Location: privado.php" );
}
function devuelveReceta($id) {
	$conexion = conectar ();
	$conexion->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	$sql = "select * FROM appRecetario_receta where id = '" . $id . "'";
	$result = $conexion->prepare ( $sql );
	$result->execute ();
	$conexion = null;
	return $result;
}
function listaReceta($receta) {
	$conexion = conectar ();
	$nom = $receta . '%';
	$conexion->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	$sql = "SELECT * FROM appRecetario_receta WHERE nombre LIKE '" . $nom . "'";
	$result = $conexion->prepare ( $sql );
	$result->execute ();
	$conexion = null;
	return $result;
}
function listaRecetaEIngrediente($receta, $ingrediente) {
	$conexion = conectar ();
	$nom = $receta . '%';
	$ap = '%' . $ingrediente . '%';
	$conexion->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	$sql = "SELECT * FROM appRecetario_receta WHERE nombre LIKE '" . $nom . "' AND ingredientes LIKE '" . $ap . "'";
	$result = $conexion->prepare ( $sql );
	$result->execute ();
	$conexion = null;
	return $result;
}
function listaRecetaIngredienteYTipo($receta, $ingrediente, $tipo) {
	$conexion = conectar ();
	$nom = $receta . '%';
	$ap = $ingrediente . '%';
	$cor = $tipo . "%";
	$sql = "SELECT * FROM appRecetario_receta WHERE nombre LIKE '" . $nom . "' AND ingredientes LIKE '" . $ap . "' AND tipo LIKE '" . $cor . "'";
	$result = $conexion->prepare ( $sql );
	$result->execute ();
	$conexion = null;
	return $result;
}
function listaPorTodo($receta, $ingrediente, $tipo) {
	$conexion = conectar ();
	$nom = $nombre . '%';
	$cor = $ingrediente . "%";
	$tip = $tipo;
	$cre = $creador;
	
	$sqLId = "SELECT id from auth_user WHERE username LIKE '" . $cre . "';";
	$result = $conexion->prepare ( $sql );
	$id = $result->execute ();
	$conexion = null;
	
	$conexion->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	$sql = "SELECT * FROM appRecetario_receta WHERE nombre LIKE '" . $nom . "' AND ingredientes LIKE '" . $cor . "' AND tipo LIKE '" . $tip . "' AND usuario_id = '" . $id . "';";
	$result = $conexion->prepare ( $sql );
	$result->execute ();
	$conexion = null;
	return $result;
}
function listaRecetaYTipo($receta, $tipo) {
	$conexion = conectar ();
	$ap = $receta . '%';
	$cor = $tipo . "%";
	
	$sql = "SELECT * FROM appRecetario_receta WHERE nombre LIKE '" . $ap . "' AND tipo LIKE '" . $cor . "'";
	$result = $conexion->prepare ( $sql );
	$result->execute ();
	$conexion = null;
	return $result;
}
function listaIngredienteYTipo($ingrediente, $tipo) {
	$conexion = conectar ();
	$ap = '%' . $ingrediente . '%';
	$cor = $tipo . "%";
	
	$sql = "SELECT * FROM appRecetario_receta WHERE ingredientes LIKE '" . $ap . "' AND tipo LIKE '" . $cor . "'";
	$result = $conexion->prepare ( $sql );
	$result->execute ();
	$conexion = null;
	return $result;
}
function listaIngredientes($ingredientes) {
	$conexion = conectar ();
	$ap = '%' . $ingredientes . '%';
	$sql = "SELECT * FROM appRecetario_receta WHERE ingredientes LIKE '" . $ap . "'";
	$result = $conexion->prepare ( $sql );
	$result->execute ();
	$conexion = null;
	return $result;
}
function listaTipos($tipo) {
	$conexion = conectar ();
	$ap = $tipo . '%';
	
	$sql = "SELECT * FROM appRecetario_receta WHERE tipo LIKE '" . $ap . "'";
	$result = $conexion->prepare ( $sql );
	$result->execute ();
	$conexion = null;
	return $result;
}

/**
 * Destruye la sesion actual.
 * Es invocado en logout.php
 */
function logout() {
	unset ( $_SESSION ['user'] );
	session_destroy ();
}
function cifra_passwd($passwd) {
	if (function_exists ( "password_hash" )) {
		return password_hash ( $passwd, PASSWORD_DEFAULT );
	} else {
		return crypt ( $passwd );
	}
}
function comprueba_passwd($passwd, $cifrada) {
	if (function_exists ( "password_hash" )) {
		return password_verify ( $passwd, $cifrada );
	} else {
		return crypt ( $passwd, $cifrada ) == $cifrada;
	}
}
function verificar($password, $hashedPassword) {
	return crypt ( $password, $hashedPassword ) == $hashedPassword;
}

/**
 * Utilizado para sustituir caracteres de distintos sistemas de codificacion en entidades HTML.
 * Por ejemlo, el simbolo < o las tildes
 * 
 * @param string $val        	
 * @return string
 */
function clean($val) {
	return htmlentities ( strip_tags ( trim ( $val ) ), ENT_QUOTES, 'UTF-8' );
}
?>