<?php

/**
 * Archivo de login. Crea las cookies del usuario si este envia el formulario con sus datos de acceso
 * @author Jose Francisco Artigas Alc�zar
 * @version 1.0
 */
require_once 'lib/control.php';
require_once 'lib/config.php';
//require_once 'lib/PasswordHash.php';

if (isset ( $_POST ['nombreLogin'] ) && isset ( $_POST ['passwordLogin'] )) {
	
	$expire = time () + 60 * 60 * 24 * 365;
	
	session_start ();
	$user = clean ( $_POST ['nombreLogin'] );
	$pass = clean ( $_POST ['passwordLogin'] );
	$num_recetas_actual = getNumRecetas ();
	$id = ( int ) $num_recetas_actual;
	$id_usuario = $id + 1;
	
	$pass_cifrada = generateHash ( $pass );
	
	setcookie ( "nickUser", $user, $expire );
	setcookie ( "passUser", $pass_cifrada, $expire );
	setcookie ( "idUser", $id_usuario, $expire );
	$_SESSION ['user'] = $user;
	
	header ( "Location: index.php" );
} else {
	$twig = config_twig ();
	$template = $twig->loadTemplate ( "login.html" );
	$datos = array ();
	echo $template->render ( $datos );
}
?>