<?php
/**
 * Archivo que recibe datos de la receta seleccionada y los envia al archivo de control para devolver el detalle de
 * la receta
 * @author Jose Francisco Artigas Alc�zar
 * @version 1.0
 */
require_once 'lib/control.php';
require_once 'lib/config.php';

if (isset ( $_GET ['id'] )) {
	$dato = devuelveReceta ( $_GET ['id'] );
	
	foreach ( $dato as $d ) {
		$dato = array (
				'id' => $d [0],
				'usuario_id' => $d [1],
				'nombre' => $d [2],
				'tipo' => $d [3],
				'elaboracion' => $d [4],
				'tiempo_aprox_elab' => $d [5],
				'fecha_creacion' => $d [6],
				'fecha_ult_mod' => $d [7],
				'ingredientes' => $d [8],
				'foto' => $d [9] 
		);
	}
	$twig = config_twig ();
	$template = $twig->loadTemplate ( "detalle.html" );
	$datos = array (
			'dato' => $dato 
	);
	echo $template->render ( $datos );
}

?>