Recetario PHP
=============
:Autor: Jose Francisco Artigas Alcázar
:Version: 1.0

.. header:

*El recetario en PHP es una aplicacion web que le permite almacenar recetas de cocina, asi como borrarlas, editarlas o modificarlas.* 

*Esta escrita en PHP y utiliza una base de datos SQLite.*

*Asimismo utiliza el motor de plantillas llamado Twig*

.. body:

Instalacion
-----------

* En Eclipse: file, import, Git, Clone URI, https://JFAABitDevelope@bitbucket.org/JFAABitDevelope/recetariophp.git
* Indique usuario y contraseńa.
* Se le pedira una ubicacion para el proyecto

Twig
----
Twig es un motor de plantillas html. Necesitara descargarlo. Si tiene **curl** instalado en su sistema, puede saltarse los **dos** siguientes pasos.

* sudo apt-get install curl **para ubuntu** 
* http://curl.haxx.se/download.html **para windows**. Elija la version con soporte ssl
	* Abra un terminal. Instale Twig via *composer*:
	* Para ello navegue hasta la raiz del proyecto desde la consola
	* Escriba: **curl -s http://getcomposer.org/installer | php** (Es necesario ańadir php a la variable de entorno path en windows si no la ha ańadido ya)
* Cuando termine el proceso del paso anterior, esciba **php composer.phar require "twig/twig:~1.0" en Windows**

Base de datos
-------------
No es necesario instalar una base datos porque se adjunta el archivo. Hay dos usuarios registrados, fran y joseluis, con los que no debera logearse.

Cookies
-------
Esta aplicacion maneja **cookies** para la gestion de recetas de usuarios logeados. 

Sesiones
--------
Esta aplicacion crea una cookie *sesion* para futuras sesiones. Presione *Logout* en el menu de navegacion si quiere eliminar la sesion creada, **asi como las cookies**. ĄCUIDADO! Si presiona *Logout* no podra modificar sus recetas en futuras sesiones. Si cierra el navegador, el sistema recordara su identificacion para futuras sesiones, y podra modificar sus recetas

Uso
---
La aplicacion es intuitiva y solo tiene 4 items de navegacion:

* **Home**: muestra un listado de todas las recetas ordenadas por fecha de creacion, de la mas reciente a la mas antigua.
* **My recypes**: Muestra una lista de sus recetas, que podrá borrar modificar o ańadir a la lista.
* **Cooking**: Es un atajo le permite crear nuevas recetas. 
* **Logout**: Destruye la sesion actual.

URL`s amigables
---------------
Se utiliza un archivo .htaccess para establecer patrones que permitan el uso y produccion de url´s amigables.

*ĄQue aproveche!*

