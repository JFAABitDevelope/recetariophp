<?php

/**
 * Archivo de logout. Invoca a la funcion logout() del archivo de control para destruir la sesion actual. Tambi�n
 * destruye las cookies de usuario
 * @author Jose Francisco Artigas Alc�zar
 * @version 1.0
 */
include "lib/control.php";
ini_set ( "error_reporting", "true" );
error_reporting ( E_ALL | E_STRICT );
session_start ();
setcookie ( "nickUser", $user, time () - 3600 );
setcookie ( "passUser", $pass_cifrada, time () - 3600 );
setcookie ( "idUser", $id_usuario, - 3600 );
logout ();
header ( "Location: index.php" );
?>