<?php

/**
 * Archivo qu env�a datos al archivo de control para realizar modificaciones en recetas.
 * @author Jose Francisco Artigas Alc�zar
 * @version 1.0
 */
include ("lib/config.php");
include ("lib/control.php");

if (isset ( $_GET ['id'] )) {
	$receta = devuelveReceta ( $_GET ['id'] );
	while ( $resultado = $receta->fetch () ) {
		$datos = array (
				'id' => $resultado [0],
				'nombre' => $resultado [2],
				'tipo' => $resultado [3],
				'elaboracion' => $resultado [4],
				'tiempo_aprox_elab' => $resultado [5],
				'ingredientes' => $resultado [8] 
		);
	}
	$twig = config_twig ();
	$template = $twig->loadTemplate ( "editar.html" );
	echo $template->render ( array (
			'id' => $_GET ['id'],
			"datos" => $datos 
	) );
} else {
	if (isset ( $_POST ['iId'] ) && isset ( $_POST ['inputNombre'] ) && isset ( $_POST ['inputTipo'] ) && isset ( $_POST ['inputElaboracion'] ) && isset ( $_POST ['inputTiempoAprox'] ) && isset ( $_POST ['inputIngredientes'] )) {
		$id = $_POST ['iId'];
		$nombre = $_POST ['inputNombre'];
		$tipo = $_POST ['inputTipo'];
		$elaboracion = $_POST ['inputElaboracion'];
		$tiempoAprox = $_POST ['inputTiempoAprox'];
		$ingredientes = $_POST ['inputIngredientes'];
		modificarReceta ( $id, $nombre, $tipo, $elaboracion, $tiempoAprox, $ingredientes );
	}
}
?>