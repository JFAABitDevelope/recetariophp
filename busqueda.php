<?php

/**
 * Archivo de busqueda
 * Permite enviar datos recogidos desde el formulario de busqueda al archivo de control para la ejecucion de
 * consultas y devolucion de coincidencias
 * @author Jose Francisco Artigas Alc�zar
 * @version 1.0
 */
require_once 'lib/control.php';
require_once 'lib/config.php';

if (isset ( $_POST ['receta'] ) && isset ( $_POST ['ingrediente'] ) && isset ( $_POST ['tipo'] )) {
	$receta = $_POST ['receta'];
	$ingrediente = $_POST ['ingrediente'];
	$tipo = $_POST ['tipo'];
	
	if ($receta == '' && $ingrediente == '' && $tipo == '') {
		echo "<div class='dAlerta'><p>No se encontraron recetas sin nombre</p></div>";
	} elseif ($receta != '' && $ingrediente == '' && $tipo == '') {
		$datos = listaReceta ( $_POST ['receta'] );
	} elseif ($receta != '' && $ingrediente != '' && $tipo == '') {
		$datos = listaRecetaEIngrediente ( $_POST ['receta'], $_POST ['ingrediente'] );
	} elseif ($receta != '' && $ingrediente != '' && $tipo != '') {
		$datos = listaRecetaIngredienteYTipo ( $_POST ['receta'], $_POST ['ingrediente'], $_POST ['tipo'] );
	} elseif ($receta != '' && $ingrediente != '' && $tipo != '') {
		$datos = listaPorTodo ( $_POST ['receta'], $_POST ['ingrediente'], $_POST ['tipo'] );
	} elseif ($receta != '' && $ingrediente == '' && $tipo != '') {
		$datos = listaRecetaYTipo ( $_POST ['receta'], $_POST ['tipo'] );
	} elseif ($receta == '' && $ingrediente != '' && $tipo != '') {
		$datos = listaIngredienteYTipo ( $_POST ['ingrediente'], $_POST ['tipo'] );
	} elseif ($receta == '' && $ingrediente != '' && $tipo == '') {
		$datos = listaIngredientes ( $_POST ['ingrediente'] );
	} elseif ($receta == '' && $ingrediente == '' && $tipo != '') {
		$datos = listaTipos ( $_POST ['tipo'] );
	}
	
	$twig = config_twig ();
	$template = $twig->loadTemplate ( "lista_encontrados.html" );
	echo $template->render ( array (
			"datos" => $datos 
	) );
} else {
	$datoid = listatodosId ( $_GET ['id'] );
	$twig = config_twig ();
	$template = $twig->loadTemplate ( "lista_encontrados.html" );
	echo $template->render ( array (
			"datoid" => $datoid 
	) );
}
?>